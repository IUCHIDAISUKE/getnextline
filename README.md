# GetNextLine

This is function like `getline()`.  
I developed it with certain constraints.

```shell
Each function must be maximum 25 lines.
A function can take 4 named parameters maximum.
You can’t declare more than 5 variables per bloc.

And This is important, but `for, do..while, switch, case, goto` is not used.

and more... 
```

So maybe, it maybe make readability worse...  
Sorry ... and good luck :)

## contents

## Execution environment

```shell
Mac OS :Big Sur
$ clang -v
Apple clang version 12.0.0 (clang-1200.0.32.28)
Target: x86_64-apple-darwin20.2.0
Thread model: posix
InstalledDir: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin
```

<!-- ## Usage

```shell
$git clone 
```
You can use this function... -->


## Constraints of functions

When I created getnextline, I used the following functions.

```shell
malloc, free, read
```

## What is this ?

My function is defined below.  

```c
int32_t get_next_line(int32_t fd, uint8_t **line)
```

At first, about the arguments, the first argument is the file descriptor.  
The second argument is 1-line to be written that was read.  
And then there are 3 return value, `1`, `0`, and `-1`, respectively.  

`1` is 1-line has been read.  
`0` is `EOF` has been reached.  
`-1` is the error value.(Error in `malloc` or `read`.)  

Okay, let's start.

## 


## material
