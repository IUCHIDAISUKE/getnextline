#!/bin/zsh

clang -Wall -Werror -Wextra get_next_line.c main.c get_next_line_utils.c -D BUFFER_SIZE=$1 && ./a.out && rm -rf ./a.out