/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 17:41:13 by mchardin          #+#    #+#             */
/*   Updated: 2021/02/11 17:23:31 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "get_next_line.h"

int32_t ft_strlen(uint8_t *str)
{
	int32_t l = 0;
	while (*(str + l))
		l++;
	return l;
}

int main()
{
	int32_t fd, i;
	uint8_t *line = 0;

	printf("\n==========================================\n");
	printf("===============  test file1 ==============\n");
	printf("==========================================\n\n");

	if (!(fd = open("test.txt", O_RDONLY)))
	{
		printf("\nError in open\n");
		return (0);
	}
	while ((i = get_next_line(fd, &line)) > 0)
	{
		printf("%d | l = %3d|%s\n", i, ft_strlen(line), line);
		free(line);
	}
	printf("%d | l = %3d|%s\n", i, ft_strlen(line), line);
	free(line);
	close(fd);
	// system("leaks a.out");

	printf("\n==========================================\n");
	printf("===============  test file2 ==============\n");
	printf("==========================================\n\n");
	// g_i = -10;
	if (!(fd = open("test2.txt", O_RDONLY)))
	{
		printf("\nError in open\n");
		return (0);
	}
	while ((i = get_next_line(fd, &line)) > 0)
	{
		// printf("--------------------------------------------\n");
		printf("%4d|%s\n", i, line);
		// printf("--------------------------------------------\n");
		// system("leaks a.out");
		free(line);
	}
	printf("%4d|%s\n", i, line);
	free(line);
	close(fd);
}