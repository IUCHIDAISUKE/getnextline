#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#define NEWLINE '\n'

typedef struct s_string t_string;
struct s_string
{
	uint8_t *m_data;
	int32_t m_size;
	void (*init)(t_string *str, int32_t);
	void (*join)(t_string *str, uint8_t *, int32_t);
	uint8_t *(*sep)(t_string *str, int);
	uint8_t *(*copy)(t_string *str);
};

void init(t_string *str, int32_t size);
void join(t_string *str, uint8_t *buff, int32_t size);
uint8_t *sep(t_string *str, int32_t num);
t_string new_string();

int32_t ck_newline(uint8_t *str, uint8_t c);
int32_t get_next_line(int32_t fd, uint8_t **line);

typedef enum
{
	ERR_ = -1,
	EOF_,
	SUC_,
} t_result;
#endif
