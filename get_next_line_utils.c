#include "get_next_line.h"

uint8_t *sep(t_string *str, int32_t num)
{
	uint8_t *res;
	uint8_t *tmp;
	int32_t i;

	res = str->m_data;
	if (!(tmp = (uint8_t *)malloc(sizeof(uint8_t) * (str->m_size - num))))
		return (NULL);
	i = 0;
	while (*(res + i + num + 1))
	{
		*(tmp + i) = *(res + i + num + 1);
		i++;
	}
	*(tmp + i) = '\0';
	*(res + num) = '\0';
	str->m_data = tmp;
	str->m_size -= num + 1;
	return (res);
}

void init(t_string *str, int32_t size)
{
	str->m_size = size;
	if (!(str->m_data = (uint8_t *)malloc(sizeof(uint8_t) * (str->m_size + 1))))
		return;
	*(str->m_data) = '\0';
}

void join(t_string *str, uint8_t *buff, int32_t size)
{
	uint8_t *tmp;
	uint8_t *cur;

	tmp = str->m_data;
	if (!(cur = (uint8_t *)malloc(sizeof(uint8_t) * (str->m_size + size + 1))))
	{
		free(tmp);
		return;
	}
	while (tmp && *tmp)
		*cur++ = *tmp++;
	while (buff && *buff)
		*cur++ = *buff++;
	*cur = '\0';
	free(tmp - str->m_size);
	str->m_size += size;
	cur -= str->m_size;
	str->m_data = cur;
	return;
}

uint8_t *copy(t_string *str){
	uint8_t *res;
	uint8_t *tmp;

	if (!(res = (uint8_t *)malloc(sizeof(uint8_t) * (str->m_size + 1))))
	{
		free(str->m_data);
		return(NULL);
	}
	tmp = str->m_data;
	while (tmp && *tmp)
		*res++ = *tmp++;
	*res = '\0';
	str->m_data = NULL;
	return (res - str->m_size);
}

t_string new_string()
{
	t_string string_;

	string_.m_data = NULL;
	string_.m_size = 0;
	string_.init = init;
	string_.join = join;
	string_.sep = sep;
	string_.copy = copy;
	return (string_);
}

