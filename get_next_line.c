#include "get_next_line.h"

bool init_error(int32_t fd, uint8_t **line)
{
	uint8_t *buff;

	buff = NULL;
	if (fd < 0 || BUFFER_SIZE <= 0 || !line)
		return (true);
	if (read(fd, buff, 0) == -1)
		return (true);
	return (false);
}

int32_t ck_newline(uint8_t *str, uint8_t c)
{
	uint8_t *tmp;

	tmp = str;
	while (str && *str)
	{
		if (*str++ == c)
			return (str - tmp);
	}
	return (0);
}

int32_t get_next_line(int32_t fd, uint8_t **line)
{
	int32_t ret;
	uint32_t len;
	uint8_t *buff;
	static t_string string;

	if (init_error(fd, line))
		return (ERR_);
	if (!(buff = (uint8_t *)malloc(sizeof(uint8_t) * (BUFFER_SIZE + 1))))
		return (ERR_);
	if (!string.m_data)
		string = new_string();
	while (!(ret = ck_newline(string.m_data, NEWLINE)) &&
		   (len = read(fd, buff, BUFFER_SIZE)))
	{
		buff[len] = '\0';
		string.join(&string, buff, len);
		if (!string.m_data)
			return (ERR_);
	}
	free(buff);
	if (ret)
		return (!(*line = (uint8_t *)string.sep(&string, ret - 1)) ? ERR_ : SUC_);
	if (!ret && !len && (*line = (uint8_t *)string.copy(&string)))
		return (EOF_);
	return (ERR_);
}
